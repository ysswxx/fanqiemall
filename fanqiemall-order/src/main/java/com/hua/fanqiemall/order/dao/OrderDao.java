package com.hua.fanqiemall.order.dao;

import com.hua.fanqiemall.order.entity.OrderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单
 * 
 * @author luoweihua
 * @email lwh_559@163.com
 * @date 2021-12-09 22:05:05
 */
@Mapper
public interface OrderDao extends BaseMapper<OrderEntity> {
	
}
