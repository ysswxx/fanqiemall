package com.hua.fanqiemall.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hua.common.utils.PageUtils;
import com.hua.fanqiemall.order.entity.OrderEntity;

import java.util.Map;

/**
 * 订单
 *
 * @author luoweihua
 * @email lwh_559@163.com
 * @date 2021-12-09 22:05:05
 */
public interface OrderService extends IService<OrderEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

