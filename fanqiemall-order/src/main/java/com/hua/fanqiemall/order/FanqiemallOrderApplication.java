package com.hua.fanqiemall.order;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@MapperScan("com.hua.fanqiemall.order.dao")
@EnableDiscoveryClient
@SpringBootApplication
public class FanqiemallOrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(FanqiemallOrderApplication.class, args);
    }

}
