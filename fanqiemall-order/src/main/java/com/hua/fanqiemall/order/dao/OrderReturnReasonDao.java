package com.hua.fanqiemall.order.dao;

import com.hua.fanqiemall.order.entity.OrderReturnReasonEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 退货原因
 * 
 * @author luoweihua
 * @email lwh_559@163.com
 * @date 2021-12-09 22:05:05
 */
@Mapper
public interface OrderReturnReasonDao extends BaseMapper<OrderReturnReasonEntity> {
	
}
