package com.hua.fanqiemall.member.dao;

import com.hua.fanqiemall.member.entity.MemberLoginLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员登录记录
 * 
 * @author luoweihua
 * @email lwh_559@163.com
 * @date 2021-12-09 21:59:09
 */
@Mapper
public interface MemberLoginLogDao extends BaseMapper<MemberLoginLogEntity> {
	
}
