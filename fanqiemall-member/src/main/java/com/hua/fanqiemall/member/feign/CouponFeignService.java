package com.hua.fanqiemall.member.feign;

import com.hua.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * fanqiemall-coupon远程服务
 *
 * @author <lwh_559@163.com>
 * @since 2021/12/11 10:30
 **/
@FeignClient("fanqiemall-coupon")
public interface CouponFeignService {

    /**
     * 获取用户的优惠券
     * @author luoweihua
     * @date 2021/12/11 10:32
     * @return R
     */
    @RequestMapping("/coupon/coupon/member/list")
    R memberCoupons();
}
