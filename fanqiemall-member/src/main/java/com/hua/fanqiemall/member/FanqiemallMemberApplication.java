package com.hua.fanqiemall.member;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@MapperScan("com.hua.fanqiemall.member.dao")
@EnableDiscoveryClient
@EnableFeignClients(basePackages = "com.hua.fanqiemall.member.feign")
@SpringBootApplication
public class FanqiemallMemberApplication {

    public static void main(String[] args) {
        SpringApplication.run(FanqiemallMemberApplication.class, args);
    }

}
