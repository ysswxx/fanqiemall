package com.hua.fanqiemall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hua.common.utils.PageUtils;
import com.hua.fanqiemall.ware.entity.UndoLogEntity;

import java.util.Map;

/**
 * 
 *
 * @author luoweihua
 * @email lwh_559@163.com
 * @date 2021-12-09 22:19:28
 */
public interface UndoLogService extends IService<UndoLogEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

