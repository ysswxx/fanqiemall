package com.hua.fanqiemall.ware.dao;

import com.hua.fanqiemall.ware.entity.WareSkuEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品库存
 * 
 * @author luoweihua
 * @email lwh_559@163.com
 * @date 2021-12-09 22:19:28
 */
@Mapper
public interface WareSkuDao extends BaseMapper<WareSkuEntity> {
	
}
