package com.hua.fanqiemall.ware;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@MapperScan("com.hua.fanqiemall.ware.dao")
@EnableDiscoveryClient
@SpringBootApplication
public class FanqiemallWareApplication {

    public static void main(String[] args) {
        SpringApplication.run(FanqiemallWareApplication.class, args);
    }

}
