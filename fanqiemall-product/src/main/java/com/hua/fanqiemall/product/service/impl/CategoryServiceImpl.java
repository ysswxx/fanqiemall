package com.hua.fanqiemall.product.service.impl;

import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hua.common.utils.PageUtils;
import com.hua.common.utils.Query;

import com.hua.fanqiemall.product.dao.CategoryDao;
import com.hua.fanqiemall.product.entity.CategoryEntity;
import com.hua.fanqiemall.product.service.CategoryService;


@Service("categoryService")
public class CategoryServiceImpl extends ServiceImpl<CategoryDao, CategoryEntity> implements CategoryService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<CategoryEntity> page = this.page(
                new Query<CategoryEntity>().getPage(params),
                new QueryWrapper<CategoryEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<CategoryEntity> listWithTree() {
        List<CategoryEntity> categoryEntityList = baseMapper.selectList(null);
        if(categoryEntityList!=null && categoryEntityList.size()>0) {
             return categoryEntityList.stream().filter(
                    mean -> mean.getParentCid().equals(0L)
            ).peek(
                    mean-> mean.setChildrens(getChildrens(mean.getCatId(),categoryEntityList))
             ).sorted(
                     Comparator.comparingInt(mean -> (mean.getSort() == null ? 0 : mean.getSort()))
             ).collect(Collectors.toList());
        }
        return categoryEntityList;
    }

    /**
     * 根据id列表删除菜单
     * @author luoweihua
     * @date 2021/12/13 23:05
     * @param list
     * @return Boolean
     */
    @Override
    public Boolean removeMenuByIds(List<Long> list) {
        //TODO 检查当前删除的菜单，是否被别的地方引用
        //逻辑删除
        int i = baseMapper.deleteBatchIds(list);
        return i>0;
    }

    private List<CategoryEntity> getChildrens(Long rootId,List<CategoryEntity> allList){
        return allList.stream().filter(
                entity->entity.getParentCid().equals(rootId)
        ).peek(
                mean -> mean.setChildrens(getChildrens(mean.getCatId(),allList))
        ).sorted(
                Comparator.comparingInt(mean -> (mean.getSort() == null ? 0 : mean.getSort()))
        ).collect(Collectors.toList());
    }
}