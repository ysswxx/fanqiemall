package com.hua.fanqiemall.product.dao;

import com.hua.fanqiemall.product.entity.CategoryEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品三级分类
 * 
 * @author luoweihua
 * @email lwh_559@163.com
 * @date 2021-12-09 22:14:00
 */
@Mapper
public interface CategoryDao extends BaseMapper<CategoryEntity> {
	
}
