package com.hua.fanqiemall.product;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@MapperScan("com.hua.fanqiemall.product.dao")
@EnableDiscoveryClient
@SpringBootApplication
public class FanqiemallProductApplication {

    public static void main(String[] args) {
        SpringApplication.run(FanqiemallProductApplication.class, args);
    }

}
