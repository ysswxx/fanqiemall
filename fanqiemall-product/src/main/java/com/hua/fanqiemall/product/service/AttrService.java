package com.hua.fanqiemall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hua.common.utils.PageUtils;
import com.hua.fanqiemall.product.entity.AttrEntity;

import java.util.Map;

/**
 * 商品属性
 *
 * @author luoweihua
 * @email lwh_559@163.com
 * @date 2021-12-09 22:14:01
 */
public interface AttrService extends IService<AttrEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

