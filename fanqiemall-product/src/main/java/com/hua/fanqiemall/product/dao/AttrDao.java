package com.hua.fanqiemall.product.dao;

import com.hua.fanqiemall.product.entity.AttrEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品属性
 * 
 * @author luoweihua
 * @email lwh_559@163.com
 * @date 2021-12-09 22:14:01
 */
@Mapper
public interface AttrDao extends BaseMapper<AttrEntity> {
	
}
