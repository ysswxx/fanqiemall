package com.hua.fanqiemall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hua.common.utils.PageUtils;
import com.hua.fanqiemall.product.entity.CategoryEntity;

import java.util.List;
import java.util.Map;

/**
 * 商品三级分类
 *
 * @author luoweihua
 * @email lwh_559@163.com
 * @date 2021-12-09 22:14:00
 */
public interface CategoryService extends IService<CategoryEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 获取商品三级分类的树形结果
     * @author luoweihua
     * @date 2021/12/12 17:07
     * @return List<CategoryEntity>
     */
    List<CategoryEntity> listWithTree();

    Boolean removeMenuByIds(List<Long> list);
}

