package com.hua.fanqiemall.product.dao;

import com.hua.fanqiemall.product.entity.AttrGroupEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 属性分组
 * 
 * @author luoweihua
 * @email lwh_559@163.com
 * @date 2021-12-09 22:14:00
 */
@Mapper
public interface AttrGroupDao extends BaseMapper<AttrGroupEntity> {
	
}
