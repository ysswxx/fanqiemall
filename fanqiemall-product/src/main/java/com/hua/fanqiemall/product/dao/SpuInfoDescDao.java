package com.hua.fanqiemall.product.dao;

import com.hua.fanqiemall.product.entity.SpuInfoDescEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * spu信息介绍
 * 
 * @author luoweihua
 * @email lwh_559@163.com
 * @date 2021-12-09 22:14:00
 */
@Mapper
public interface SpuInfoDescDao extends BaseMapper<SpuInfoDescEntity> {
	
}
