package com.hua.fanqiemall.product;

import com.hua.fanqiemall.product.entity.BrandEntity;
import com.hua.fanqiemall.product.service.BrandService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class FanqiemallProductApplicationTests {

    @Autowired
    private BrandService brandService;

    @Test
    void contextLoads() {
        BrandEntity brandEntity = new BrandEntity();
        brandEntity.setName("华为");
        brandEntity.setBrandId(1L);
        boolean b = brandService.updateById(brandEntity);
        System.out.println(b);
    }

}
