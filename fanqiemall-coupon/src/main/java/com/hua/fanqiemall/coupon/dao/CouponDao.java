package com.hua.fanqiemall.coupon.dao;

import com.hua.fanqiemall.coupon.entity.CouponEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 优惠券信息
 * 
 * @author luoweihua
 * @email lwh_559@163.com
 * @date 2021-12-09 21:52:55
 */
@Mapper
public interface CouponDao extends BaseMapper<CouponEntity> {
	
}
