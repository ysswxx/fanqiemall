package com.hua.fanqiemall.coupon;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@MapperScan("com.hua.fanqiemall.coupon.dao")
@EnableDiscoveryClient
@SpringBootApplication
public class FanqiemallCouponApplication {

    public static void main(String[] args) {
        SpringApplication.run(FanqiemallCouponApplication.class, args);
    }

}
