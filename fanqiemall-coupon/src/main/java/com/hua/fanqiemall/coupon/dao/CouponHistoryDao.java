package com.hua.fanqiemall.coupon.dao;

import com.hua.fanqiemall.coupon.entity.CouponHistoryEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 优惠券领取历史记录
 * 
 * @author luoweihua
 * @email lwh_559@163.com
 * @date 2021-12-09 21:52:54
 */
@Mapper
public interface CouponHistoryDao extends BaseMapper<CouponHistoryEntity> {
	
}
